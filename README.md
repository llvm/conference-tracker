# conference-tracker

Track upcoming conferences and submission deadlines.

Based on https://github.com/bamos/conference-tracker .

## Examples

Show upcoming conferences:

   `$ ./report.py`

CFP (deadline) information:

   `$ ./report.py --cfp`

Generate as HTML (using 'aha' tool):

   `$ ./report.py --cfp|aha>| cfp.html`

## Adding and Updating Data
As conferences pass and the data becomes outdated,
I'm happy to accept pull requests with updated data.
If you want to track yours by adding/removing conferences,
please fork this repo.

## Conference Lists
+ [Wikipedia](https://en.wikipedia.org/wiki/List_of_computer_science_conferences)
+ [RichardLitt/awesome-conferences](https://github.com/RichardLitt/awesome-conferences)
+ [WikiCFP: CS](http://www.wikicfp.com/cfp/call?conference=computer%20science)