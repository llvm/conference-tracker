#!/usr/bin/env python3

import argparse
import datetime as dt
import os
import sys
import yaml

from itertools import chain
from operator import attrgetter
from termcolor import colored

today = dt.datetime.today().date()


def getExpectedField(yaml, field):
    if field not in yaml:
        print("Error: field '{}' not in {}.".format(field, yaml))
        sys.exit(-1)
    return yaml[field]


def getOptionalField(yaml, field):
    if field not in yaml:
        return ""
    return yaml[field]


class Conference:
    def __init__(self, yaml, group):
        self.group = group
        self.title = getExpectedField(yaml, 'title')
        self.date = getExpectedField(yaml, 'date')
        self.deadline = getOptionalField(yaml, 'deadline')
        self.url = getExpectedField(yaml, 'url')

    def __repr__(self):
        return self.title


class ConferenceGroup:
    def __init__(self, yaml):
        self.title = getExpectedField(yaml, 'title')
        self.conferences = [Conference(c, self.title)
                            for c in getExpectedField(yaml, 'conferences')]
        self.upcoming = [c for c in self.conferences if c.date > today]
        self.week = [c for c in self.conferences
                     if 0 <= (today - c.date).days <= 6]
        self.outdated = [c for c in self.conferences
                         if (today - c.date).days > 6]
        self.cfp = [c for c in self.conferences if c.deadline != ""]
        self.no_cfp = [c for c in self.conferences if c not in self.cfp]
        # Deadlines passed
        self.cfp_past = [c for c in self.cfp if c.deadline <= today]
        # Conference itself passed
        self.cfp_outdated = [c for c in self.cfp
                             if (today - c.deadline).days > 0]
        # Conferences without submission deadlines but upcoming
        self.no_cfp_upcoming = [c for c in self.no_cfp if c.date > today]

        quarter_days = 3*30
        self.cfp_upcoming = [c for c in self.cfp if c.deadline > today]
        self.cfp_quarter = [c for c in self.cfp
                            if 0 <= (c.deadline - today).days <= quarter_days]
        self.cfp_future = [c for c in self.cfp
                           if (c.deadline - today).days > quarter_days]

    def __repr__(self):
        return "{}: {}".format(self.title, self.conferences)


def iterGroups(dataDir):
    for group in os.listdir(args.dataDir):
        dataFile = os.path.join(dataDir, group)
        if dataFile.endswith(".yaml"):
            with open(dataFile, 'r') as f:
                yield ConferenceGroup(yaml.load(f))


def printHdr(hdr):
    print(colored("\n# " + hdr, 'blue', attrs=['bold']))


def printSeparator():
    print(colored("\n----------------------------------------", 'cyan'))


def fmtGrp(grp):
    return colored(grp, 'yellow')


def fmtDate(date):
    if (date > today):
        return colored(date, 'green', attrs=['bold'])
    else:
        return colored(date, 'red', attrs=['bold'])


def fmtTitle(title):
    return colored(title, 'cyan', attrs=['bold'])


def printConf(conf):
    print("+ [{}] {}".format(fmtGrp(conf.group), fmtTitle(conf.title)))
    print("    + {}".format(conf.url))
    print("    + {}".format(fmtDate(conf.date)))

def printDeadlineConf(conf):
    days = (conf.deadline - today).days
    print("+ [{}] {}".format(fmtGrp(conf.group), fmtTitle(conf.title)))
    print("    + {}".format(conf.url))
    print("    + S: {} ({}D/{:.1f}W/{:.1f}M)".format(fmtDate(conf.deadline),
                                                      days, days/7, days/30))
    print("    + C: {}".format(fmtDate(conf.date)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    myDir = os.path.dirname(os.path.realpath(__file__))
    parser.add_argument('--dataDir', type=str,
                        help="Directory of YAML conference data. "
                        "Defaults to '<conference tracker>/data'",
                        metavar="DIR",
                        default="{}/data".format(myDir))
    parser.add_argument('--cfp', help="Conference submission view",
                        action="store_true")
    parser.add_argument('--cfp-just-the-facts',
                        help="Don't print outdated or missing deadline info.",
                        action="store_true")
    parser.add_argument('--cfp-show-missing',
                        help="List conferences without deadline information",
                        action="store_true")
    args = parser.parse_args()

    confGroups = list(iterGroups(args.dataDir))

    if not args.cfp:
        upcoming = list(chain.from_iterable(cg.upcoming for cg in confGroups))
        outdated = list(chain.from_iterable(cg.outdated for cg in confGroups))
        week = list(chain.from_iterable(cg.week for cg in confGroups))

        printHdr("Conferences this past week.")
        for conf in week:
            printConf(conf)

        printHdr("Upcoming conferences.")
        for conf in sorted(upcoming, key=attrgetter('date')):
            printConf(conf)

        printHdr("Past conferences. Update data.")
        for conf in sorted(outdated, key=attrgetter('date'), reverse=True):
            printConf(conf)
    else:
        confGroups = list(iterGroups(args.dataDir))
        quarter = list(chain.from_iterable(cg.cfp_quarter for cg in confGroups))
        future = list(chain.from_iterable(cg.cfp_future for cg in confGroups))
        no_cfp = list(chain.from_iterable(cg.no_cfp for cg in confGroups))
        no_cfp_upcoming = list(chain.from_iterable(cg.no_cfp_upcoming
                                                   for cg in confGroups))
        outdated = list(chain.from_iterable(cg.cfp_outdated
                                            for cg in confGroups))

        printHdr("Submission deadlines in upcoming 3 months.")
        for conf in sorted(quarter, key=attrgetter('deadline')):
            printDeadlineConf(conf)

        printHdr("Submission deadlines in the future.")
        for conf in sorted(future, key=attrgetter('deadline')):
            printDeadlineConf(conf)

        if not args.cfp_just_the_facts:
            printSeparator()
            printHdr("Conferences with deadlines already passed.")
            for conf in sorted(outdated, key=attrgetter('deadline'),
                               reverse=True):
                printDeadlineConf(conf)

            # For this report, don't show old conferences w/o deadlines.
            if args.cfp_show_missing:
                printHdr("Conferences w/o deadlines, consider adding.")
                for conf in sorted(no_cfp, key=attrgetter('date')):
                    printConf(conf)
            else:
                printHdr("Upcoming Conferences w/o deadlines, consider adding.")
                for conf in sorted(no_cfp_upcoming, key=attrgetter('date')):
                    printConf(conf)
