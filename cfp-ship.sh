#!/bin/sh

TITLE="Conference Status for $(date +%Y-%m-%d)"

(echo "$TITLE"; ./report.py --cfp) | \
  aha -s -b -t "$TITLE" | \
  sed 's/3333FF/5555FF/'| \
  sed 's@\+ \(http[s]\?://[^ \t]*\)@+ <a href=\"\1\" class=\"underline white\">\1</a>@g' \
  > ./cfp.html

scp ./cfp.html w:www/files/
